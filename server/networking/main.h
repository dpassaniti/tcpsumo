#pragma once
#include "SDL.h"
#include "SDL_net.h"
#include "GameStream.h"
#include "common.h"

//sdl
SDL_Window* window = NULL;//The window
SDL_Renderer* renderer = NULL;//The window renderer

//for rendering and game over collision check
ground platform;

//struct that holds client info (both game and network related)
struct clientInfo
{
	TCPsocket socket;
	Uint32 timeout;
	bool dcd;//disconnected
	bool colliding;//don't repeat collision check until the last collision is fully resolved
	bool eliminated;//lost the game
	Minion player;
	
	clientInfo(TCPsocket sock, Uint32 to, int id)
		: socket(sock), timeout(to), dcd(false), colliding(false), eliminated(false)
	{
		player.init(vec2(SCREEN_WIDTH/2, SCREEN_HEIGHT/2), rgbaColour(ENEMY_COLOUR));
		player.setId(id);
	};
};
TCPstream TCPconnection;
TCPsocket serverSocket;
SDLNet_SocketSet clientSockets;//all connected sockets, for network operations
std::vector<clientInfo> clients;//all connected clients

//data holders
data recvData;
data sendData;
//game info
int idCount = 0;//id to assign to next client who connects
int playerCount = 0;//num of connected clients

//game loop functions
bool init();//Starts up SDL and creates window
bool handleInput();//handle input events, ret true if quit
void checkNewConnection();//check for newe incoming connection requests
void manageDataFlow();
void checkPlayerCollisions();//check collisions between players and act accordingly
void checkGroundCollisions();//check if any of the player fell out of the ground and lost
void drawScene();//draw ground and players
void close();//Frees media and shuts down SDL
//other functions
void disconnectPlayer(int vecPosition);//close connection with a client

bool checkCollision(SDL_Rect a, SDL_Rect b);//simple separating axis collision check from lazyfoo.net's SDL tutorials