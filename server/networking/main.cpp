#include "main.h"

int main(int argc, char* args[])
{
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		bool quit = false;

		while (!quit)//While application is running
		{
			quit = handleInput();//Handle events on queue

			//check if someone wants to connect
			checkNewConnection();
			
			//receive and forward players' info
			manageDataFlow();
			
			//check for timeouts
			for (int k = 0; k < clients.size(); ++k)
			{
				if (SDL_GetTicks() - clients[k].timeout > 5000)//more than 5 seconds since we received any message
				{
					printf("player timed out! ");
					clients[k].dcd = true;
				}
			}

			//check for collisions and notify interested players with opposing's velocities				
			checkPlayerCollisions();
			
			//check for losing players and notify all
			checkGroundCollisions();

			//disconnect dcd players
			for (int currClient = 0; currClient < clients.size(); ++currClient)
			{
				if (clients[currClient].dcd == true)
					disconnectPlayer(currClient);
			}

			//draw spectator view of the game
			drawScene();
		}
	}

	//Free resources and close SDL
	close();

	return 0;
}

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else if (SDLNet_Init() < 0)
	{
		printf("SDL Net could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Create window
		window = SDL_CreateWindow("Server", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (window == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create renderer for window
			renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
			if (renderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize renderer color
				SDL_SetRenderDrawColor(renderer, 50, 50, 50, 255);//dark grey
			}
		}

		//init connection
		if (!TCPconnection.open(NULL, &serverSocket, SERVERPORT))//NULL ip means we're not conntecting to anyone(we're a server)
		{
			printf("Could not initialise the connection\n", SDL_GetError());
			success = false;
		}
		else
		{
			clientSockets = SDLNet_AllocSocketSet(MAX_PLAYERS);//set max number of connected clients to 4
		}
	}

	return success;
}

bool handleInput()
{
	SDL_Event e;//Event handler

	while (SDL_PollEvent(&e) != 0)
	{
		//keydown
		if (e.type == SDL_KEYDOWN)
		{
			switch (e.key.keysym.sym)
			{
			case SDLK_ESCAPE:
				return true;
				break;
			default:
				break;
			}
		}
		//User requests quit
		else if (e.type == SDL_QUIT)
		{
			return true;
		}
	}
	return false;//no quit event
}

void disconnectPlayer(int vecPosition)
{
	data sendData;

	//notify all players except		
	sendData.messageType = (Sint8)PLAYER_QUIT;
	sendData.playerID = (Sint8)clients[vecPosition].player.getId();
	//no need to fill the rest
	//sendData.positionX = (Sint16)0;
	//sendData.positionY = (Sint16)0;
	//sendData.velocityX = (Sint16)0;
	//sendData.velocityY = (Sint16)0;
	for (int s = 0; s < clients.size(); ++s)
	{
		if(!TCPconnection.send(&sendData, &(clients[vecPosition].socket)))
			printf("FAIL notify DC\n");
	}
	//disconnect player
	printf("player %d: connection lost!\n", clients[vecPosition].player.getId());
	SDLNet_TCP_Close(clients[vecPosition].socket);
	SDLNet_TCP_DelSocket(clientSockets, clients[vecPosition].socket);
	clients.erase(clients.begin() + vecPosition);
	--playerCount;
}

void drawScene()
{
	//Clear creen
	SDL_SetRenderDrawColor(renderer, BACKGROUND_COLOUR);
	SDL_RenderClear(renderer);
	//draw ground
	SDL_SetRenderDrawColor(renderer, platform.colour.r, platform.colour.g, platform.colour.b, platform.colour.a);
	SDL_RenderFillRect(renderer, &platform.rect);
	//draw all players
	for (int i = 0; i < clients.size(); ++i)
	{
		clients[i].player.draw(renderer);
	}
	//update scren
	SDL_RenderPresent(renderer);
}

void checkNewConnection()
{
	TCPsocket testSocket = SDLNet_TCP_Accept(serverSocket);
	if (testSocket)//if testSocket is not NULL someone tried to connect
	{
		if (playerCount < MAX_PLAYERS)//add new player if there aren't too many already
		{
			//save new client data
			SDLNet_TCP_AddSocket(clientSockets, testSocket);
			clients.push_back(clientInfo(testSocket, SDL_GetTicks(), idCount));

			playerCount++;

			//send the client its new id
			sendData.messageType = (Sint8)ID_ASSIGNMENT;
			sendData.playerID = (Sint8)idCount;
			//no need to fill the rest
			//sendData.positionX = (Sint16)0;
			//sendData.positionY = (Sint16)0;
			//sendData.velocityX = (Sint16)0;
			//sendData.velocityY = (Sint16)0;

			printf("New connection! id: %d \n", idCount);
			idCount++;

			if (!TCPconnection.send(&sendData, &testSocket))//if send fails, close connection
			{
				disconnectPlayer(clients.size() - 1);
			}
		}
		else
		{
			//tell the client we're full by sending id = -1
			sendData.messageType = (Sint8)ID_ASSIGNMENT;
			sendData.playerID = (Sint8)-1;
			//no need to fill the rest
			//sendData.positionX = (Sint16)0;
			//sendData.positionY = (Sint16)0;
			//sendData.velocityX = (Sint16)0;
			//sendData.velocityY = (Sint16)0;
			printf("New connection refused, too many players\n");
			//answer request
			TCPconnection.send(&sendData, &testSocket);
			SDLNet_TCP_Close(testSocket);//close the socket
		}
	}
}

void manageDataFlow()
{
	//check for incoming data
	while (SDLNet_CheckSockets(clientSockets, 0) > 0)//loop over socket set
	{
		for (int currClient = 0; currClient < clients.size(); ++currClient)
		{
			if (SDLNet_SocketReady(clients[currClient].socket))//there's data ready
			{
				clients[currClient].timeout = SDL_GetTicks();//update timeout value to check after loop

				if (!TCPconnection.recv(&recvData, &(clients[currClient].socket)))
				{
					printf("FAIL base recv\n");
					clients[currClient].dcd = true;
				}

				else if (recvData.messageType == PLAYER_INFO)
				{
					//notify all other players
					for (int k = 0; k < clients.size(); k++)
					{
						if (k != currClient)//don't notify who sent us the data
						{
							if (!TCPconnection.send(&recvData, &(clients[k].socket)))
							{
								printf("FAIL send info\n");
								clients[k].dcd = true;
							}
						}
						else//update local copy with new position and velocity
						{
							clients[k].player.setPosition(vec2(recvData.positionX, recvData.positionY));
							clients[k].player.setVelocity(vec2(recvData.velocityX, recvData.velocityY));
						}

					}
				}
				else if (recvData.messageType == PLAYER_QUIT)//player quits game properly
				{
					//notify all other players
					for (int k = 0; k < clients.size(); ++k)
					{
						if (k != currClient)
						{
							if (!TCPconnection.send(&recvData, &(clients[k].socket)))
							{
								printf("FAIL notify quit\n");
								clients[k].dcd = true;
							}
						}
					}
					//remove disconnected player from game
					printf("player %d closed the connection!\n", clients[currClient].player.getId());
					SDLNet_TCP_Close(clients[currClient].socket);
					SDLNet_TCP_DelSocket(clientSockets, clients[currClient].socket);
					clients.erase(clients.begin() + currClient);
					--playerCount;
				}
			}
		}
	}
}
void checkPlayerCollisions()
{
	//check if a player was colliding and unset the collide flag if he's not anymore
	for (int k = 0; k < clients.size(); ++k)
	{
		if (clients[k].colliding == true)//was colliding already
		{
			for (int j = 0; j < clients.size(); ++j)//check if it's still colliding with anyone
			{
				if (clients[j].colliding == true)
					if (j != k)//avoid checking collision with self
					{
						if (!checkCollision(clients[k].player.getRect(), clients[j].player.getRect()))
						{
							//if they're not colliding anymore, unset flag
							clients[k].colliding = false;
							clients[j].colliding = false;
						}
					}
			}
		}
	}
	//check collisions between players not in collision already
	for (int k = 0; k < clients.size(); ++k)
	{
		for (int j = 0; j < clients.size(); ++j)
		{
			if (j != k)//avoid checking collision with self
			{
				if ((!clients[k].colliding) && (!clients[j].colliding))//neither is already in the middle of a collision
				{
					if (checkCollision(clients[k].player.getRect(), clients[j].player.getRect()))//if they're colliding
					{
						printf("Player %d collided with player %d!\n", clients[k].player.getId(), clients[j].player.getId());
						//notify client k with player j info
						sendData.messageType = (Sint8)COLLISION;
						sendData.playerID = (Sint8)clients[j].player.getId();
						//sendData.positionX = (Sint16)clients[j].player.getPosition().x;//irrelevant
						//sendData.positionY = (Sint16)clients[j].player.getPosition().y;//irrelevant
						sendData.velocityX = (Sint16)clients[j].player.getVelocity().x;
						sendData.velocityY = (Sint16)clients[j].player.getVelocity().y;
						if (!TCPconnection.send(&sendData, &(clients[k].socket)))
						{
							printf("FAIL notify collision\n");
							clients[k].dcd = true;
						}
						//notify client j with player k info
						sendData.messageType = (Sint8)COLLISION;
						sendData.playerID = (Sint8)clients[k].player.getId();
						//sendData.positionX = (Sint16)clients[k].player.getPosition().x;//irrelevant
						//sendData.positionY = (Sint16)clients[k].player.getPosition().y;//irrelevant
						sendData.velocityX = (Sint16)clients[k].player.getVelocity().x;
						sendData.velocityY = (Sint16)clients[k].player.getVelocity().y;
						if (!TCPconnection.send(&sendData, &(clients[j].socket)))
						{
							printf("FAIL notify collision\n");
							clients[j].dcd = true;
						}
						//set flag
						clients[k].colliding = true;
						clients[j].colliding = true;
					}
				}
			}
		}
	}
}

void checkGroundCollisions()
{
	//check collisions between players and ground
	for (int k = 0; k < clients.size(); ++k)
	{
		//if the player isn't colliding with the ground
		if (!checkCollision(clients[k].player.getRect(), platform.rect))
		{
			printf("player %d was eliminated!\n", clients[k].player.getId());
			clients[k].eliminated = true;
			//notify elimination to everyone
			for (int j = 0; j < clients.size(); ++j)
			{
				sendData.messageType = (Sint8)GAMEOVER;
				sendData.playerID = (Sint8)clients[k].player.getId();//id of eliminated player
				if (!TCPconnection.send(&sendData, &(clients[j].socket)))
				{
					printf("FAIL notify gameover\n");
					clients[j].dcd = true;
				}
			}		
		}
	}

	//remove eliminated players from game
	for (int k = 0; k < clients.size(); ++k)
	{
		if (clients[k].eliminated == true)
		{
			SDLNet_TCP_Close(clients[k].socket);
			SDLNet_TCP_DelSocket(clientSockets, clients[k].socket);
			clients.erase(clients.begin() + k);
			--playerCount;
		}		
	}	
}

void close()
{
	//notify players they're being disconnected
	for (int i = 0; i < clients.size(); ++i)
	{
		sendData.messageType = (Sint8)SERVER_QUIT;
		if (!TCPconnection.send(&sendData, &(clients[i].socket)))
		{
			printf("FAIL notify server quit\n");
			clients[i].dcd = true;
		}
	}

	//disconnect all players and free sockets
	for (int i = 0; i < clients.size(); ++i)
		SDLNet_TCP_Close(clients[i].socket);
	
	TCPconnection.close(&serverSocket);
	SDLNet_FreeSocketSet(clientSockets);

	//free window and renderer	
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	window = NULL;
	renderer = NULL;

	//Quit SDL subsystems
	SDLNet_Quit();
	SDL_Quit();
}

//simple separating axis collision check from lazyfoo.net's SDL tutorials
bool checkCollision(SDL_Rect a, SDL_Rect b)
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;

	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	//If any of the sides from A are outside of B
	if (bottomA <= topB)
	{
		return false;
	}

	if (topA >= bottomB)
	{
		return false;
	}

	if (rightA <= leftB)
	{
		return false;
	}

	if (leftA >= rightB)
	{
		return false;
	}

	//If none of the sides from A are outside B
	return true;
}