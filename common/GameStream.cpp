#include "GameStream.h"

GameStream::GameStream()
{	
}
GameStream::~GameStream()
{
}

bool GameStream::open(const char* ipAddress)
{
	bool result = true;

	if (tcps.open(ipAddress, &serverSocket, SERVERPORT))
	{
		//add to socket set
		server = SDLNet_AllocSocketSet(1);
		SDLNet_TCP_AddSocket(server, serverSocket);
	}
	else
		result = false;

	return result;
}

bool GameStream::send(Minion* player)
{
	if (player->isReady())
	{
		//fill the data to send
		sendData.messageType = (Sint8)PLAYER_INFO;
		sendData.playerID = (Sint8)player->getId();
		sendData.positionX = (Sint16)player->getPosition().x;
		sendData.positionY = (Sint16)player->getPosition().y;
		sendData.velocityX = (Sint16)player->getVelocity().x;
		sendData.velocityY = (Sint16)player->getVelocity().y;

		//send data to server
		if (!tcps.send(&sendData, &serverSocket))//couldn't send
			return false;

		return true;
	}
}

bool GameStream::recv(std::vector<Minion*>& enemies, Minion* player)
{
	while ((SDLNet_CheckSockets(server, 0) > 0) && SDLNet_SocketReady(serverSocket))//only if there is data
	{
		if (!tcps.recv(&recvData, &serverSocket))
			return false;

		if (recvData.messageType == ID_ASSIGNMENT)
		{
			if (recvData.playerID != -1)//server sent us a valid id
			{
				player->setId(recvData.playerID);
				printf("received %d as id!\n", recvData.playerID);
			}
			else//connection refused because too many players are already in game
			{
				printf("Sorry, this server is full; press esc to quit, relaunch in a bit and hope you will fit!\n");
				//close connection
				tcps.close(&serverSocket);
				SDLNet_FreeSocketSet(server);
			}
		}
		else if (recvData.messageType == PLAYER_INFO)
		{
			//received data
			vec2 recvPos((float)recvData.positionX, (float)recvData.positionY);
			vec2 recvVel((float)recvData.velocityX, (float)recvData.velocityY);

			int currEnemy;//iterator
			for (currEnemy = 0; currEnemy < enemies.size(); ++currEnemy)
			{
				//update correct enemy with received info
				if (enemies[currEnemy]->getId() == recvData.playerID)
				{
					enemies[currEnemy]->setPosition(recvPos);
					enemies[currEnemy]->setVelocity(recvVel);
					printf("enemy %d updated!\n", recvData.playerID);
					break;
				}
			}			
			if (currEnemy >= enemies.size())//there's a new player
			{
				enemies.push_back(new Minion());
				//init new minion
				enemies[currEnemy]->init(recvPos, rgbaColour(ENEMY_COLOUR));
				enemies[currEnemy]->setId(recvData.playerID);
				enemies[currEnemy]->setVelocity(recvVel);
				printf("new player connected! id: %d\n", recvData.playerID);
			}
		}
		else if (recvData.messageType == COLLISION)
		{
			//swap velocities between colliding players
			vec2 recvVel((float)recvData.velocityX, (float)recvData.velocityY);
			player->setVelocity(recvVel);
			//find correct enemy
			for (int i = 0; i < enemies.size(); ++i)
			{
				if (enemies[i]->getId() == recvData.playerID)
				{
					enemies[i]->setVelocity(player->getVelocity());
					break;
				}
			}
		}
		else if (recvData.messageType == PLAYER_QUIT)//player quit the game
		{
			if (recvData.playerID == player->getId())//we are being disconnected (should never happen this way)
			{
				printf("You are being kicked from the server! press esc to quit, check your network and relaunch\n");
				//close connection
				tcps.close(&serverSocket);
				SDLNet_FreeSocketSet(server);
				player->setId(-1);
			}
			else
			{
				//delete enemy from vector
				for (int i = 0; i < enemies.size(); ++i)
				{
					if (enemies[i]->getId() == recvData.playerID)
						enemies.erase(enemies.begin() + i);
				}
				printf("player %d left the game!\n", recvData.playerID);
			}
		}
		else if (recvData.messageType == GAMEOVER)//someone lost
		{
			if (recvData.playerID == player->getId())//we lost
			{
				printf("You were eliminated! press esc to quit\n");
				//close connection
				tcps.close(&serverSocket);
				SDLNet_FreeSocketSet(server);
				//unset id to not trigger close notification
				player->setId(-1);
			}
			else
			{
				//delete enemy from vector
				for (int i = 0; i < enemies.size(); ++i)
				{
					if (enemies[i]->getId() == recvData.playerID)
						enemies.erase(enemies.begin() + i);
				}
				printf("player %d was eliminated!\n", recvData.playerID);
			}			
		}
		else if (recvData.messageType == SERVER_QUIT)//server is shutting down
		{
			printf("The server was closed or crashed. Press esc to quit\n");
			//close connection
			tcps.close(&serverSocket);
			SDLNet_FreeSocketSet(server);
			//unset id to not trigger close notification
			player->setId(-1);
		}
	}

	return true;
}

void GameStream::close(Minion* player)
{
	//fill data with quit info
	sendData.messageType = (Sint8)PLAYER_QUIT;
	//no need to fill the rest but...
	sendData.playerID = (Sint8)player->getId();
	sendData.positionX = (Sint16)player->getPosition().x;
	sendData.positionY = (Sint16)player->getPosition().y;
	sendData.velocityX = (Sint16)player->getVelocity().x;
	sendData.velocityY = (Sint16)player->getVelocity().y;
	//notify server we're leaving
	tcps.send(&sendData, &serverSocket);

	//close connection
	tcps.close(&serverSocket);
	SDLNet_FreeSocketSet(server);
}