#pragma once
#include "SDL_net.h"
#include <stdio.h>
#include "common.h"

class TCPstream
{
public:
	TCPstream();
	~TCPstream();
	//resolve host and open TCP connection, return true if success
	bool open(const char* ipAddress, TCPsocket* socket, Uint16 port);

	//convert data to network byte order and send, ret true if success
	bool send(data* sdata, TCPsocket* socket);

	//BLOCKING, receive data and convert to correct byte order, ret true if succes
	bool recv(data* rdata, TCPsocket* socket);

	//close/free sockets
	void close(TCPsocket* socket);
};