//information useful to both client and server
#pragma once
#include "SDL.h"

//maximum number of clients accepted
#define MAX_PLAYERS 4

//Screen dimensions
#define SCREEN_WIDTH 700
#define SCREEN_HEIGHT 400

//colours
#define BACKGROUND_COLOUR  204, 20, 76, 255//60, 20, 100, 255
#define GROUND_COLOUR  20, 20, 20, 255
#define ENEMY_COLOUR 204, 127, 51, 255//234, 140, 109,255
#define PLAYER_COLOUR 0, 178, 255, 255//186, 238, 104, 255

//player dimensions
#define PLAYER_WIDTH 30
#define PLAYER_HEIGHT 30
#define PLAYER_SPEED 7

//server data
#define SERVERIP "127.0.0.1"
#define SERVERPORT 1234

//network message types
#define ID_ASSIGNMENT 0//give player an id at the start of the connection
#define PLAYER_INFO 1//player position and velocity
#define COLLISION 2//collision with enemy indicated by id
#define PLAYER_QUIT 3//a player disconnected
#define SERVER_QUIT 4//server is shutting down
#define GAMEOVER 5//a player was eliminated

//network data structure
#pragma pack(push, 1)//set byte alignment to none... but actually this isn't doing anything here since 8+8=16
struct data
{
	Sint8 messageType;//0 to 5
	Sint8 playerID;//-1 to 127;reset to 0 only on server restart, but it's unlikely to reach 127 connections in this context
	Sint16 positionX;//100 to SCREEN_WIDTH - 100
	Sint16 positionY;//100 to SCREEN_HEIGHT - 100
	Sint16 velocityX;//-? to ?; impossible to exceed +/-((2^16)/2) without losing
	Sint16 velocityY;//-? to ?; impossible to exceed +/-((2^16)/2) without losing
};
#pragma pack(pop)//reset to default

struct vec2//2d vector for position, velocity, ...
{
	float x;
	float y;

	vec2() : x(0), y(0){}
	vec2(float v1, float v2) : x(v1), y(v2) {};
};


struct rgbaColour//red green blue alpha of players
{
	int r;
	int g;
	int b;
	int a;

	rgbaColour() : r(0), g(0), b(0), a(255) {};
	rgbaColour(int v1, int v2, int v3, int v4) : r(v1), g(v2), b(v3), a(v4) {};
};

struct KEYFLAGS//keep track of what keys are pressed
{
	bool up = false;
	bool down = false;
	bool left = false;
	bool right = false;
	bool space = false;
	bool zero = false;
};

//rect on which the players are moving and must not fall out of
struct ground
{
	SDL_Rect rect;
	rgbaColour colour;
	ground()
	{
		colour = rgbaColour(GROUND_COLOUR);
		//set position on the screen
		rect = { 50, 50, SCREEN_WIDTH - 100, SCREEN_HEIGHT - 100 };
	};
};