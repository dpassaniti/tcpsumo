#include "TCPstream.h"

TCPstream::TCPstream()
{
}
TCPstream::~TCPstream()
{
}
bool TCPstream::open(const char* ipAddress, TCPsocket* socket, Uint16 port)
{
	bool result = true;

	IPaddress ip;
	if (SDLNet_ResolveHost(&ip, ipAddress, port) == -1)
	{
		printf("connection error\n");
		result = false;
	}
	else
		printf("host resolved!\n");

	*socket = SDLNet_TCP_Open(&ip);
	if (*socket == NULL)
	{
		printf("connection error (server not found)\n");
		result = false;
	}
	else
		printf("Socket opened successfully!\n");

	return result;
}

bool TCPstream::send(data* sdata, TCPsocket* socket)
{
	data beData;//sdata converted to big endian

	//convert to network byte order(big endian), unless we're on a big endian system
	if (SDL_BYTEORDER == SDL_LIL_ENDIAN)
	{
		beData.messageType = sdata->messageType;//single byte field
		beData.playerID = sdata->playerID;//single byte field
		beData.positionX = SDL_Swap16(sdata->positionX);
		beData.positionY = SDL_Swap16(sdata->positionY);
		beData.velocityX = SDL_Swap16(sdata->velocityX);
		beData.velocityY = SDL_Swap16(sdata->velocityY);
	}
	
	if (SDLNet_TCP_Send(*socket, &beData, sizeof(beData)) < sizeof(beData))//couldn't send all the data
	{
		//sdl already loops until all data is sent
		//so if we're here "the remote connection was closed, or an unknown socket error occurred"
		printf("SEND: the remote connection was closed, or an unknown socket error occurred\n");
		return false;
	}
	return true;
}

bool TCPstream::recv(data* rdata, TCPsocket* socket)
{
	int bytesReceived = 0;
	do
	{
		bytesReceived += SDLNet_TCP_Recv(*socket, rdata + bytesReceived, sizeof(*rdata));
		if (bytesReceived <= 0)//either the remote connection was closed, or an unknown socket error occurred
		{
			printf("RECV: the remote connection was closed, or an unknown socket error occurred\n");
			return false;
		}
	} while (bytesReceived < sizeof(*rdata));//loop until we receive the whole message
	
	//convert from network byte order to little endian; if our system is big endian SDL_SwapBE16 will do nothing
	rdata->messageType = rdata->messageType;//single byte field
	rdata->playerID = rdata->playerID;//single byte field
	rdata->positionX = SDL_SwapBE16(rdata->positionX);
	rdata->positionY = SDL_SwapBE16(rdata->positionY);
	rdata->velocityX = SDL_SwapBE16(rdata->velocityX);
	rdata->velocityY = SDL_SwapBE16(rdata->velocityY);

	return true;
}

void TCPstream::close(TCPsocket* socket)
{
	printf("bye bye!\n");
	SDLNet_TCP_Close(*socket);
}