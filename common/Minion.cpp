#include "minion.h"

Minion::Minion()
{}

Minion::~Minion()
{}

void Minion::init(vec2 pos, rgbaColour colr)
{
	velocity.x = 0;
	velocity.y = 0;

	playerId = - 1;// not set

	position = pos;

	colour = colr;

	shape = { position.x, position.y, PLAYER_WIDTH, PLAYER_HEIGHT };
}

void Minion::update(KEYFLAGS keysdown, float timeStep)
{
	//get velocity input
	if (keysdown.up)
	{
		velocity.y -= PLAYER_SPEED;
	}
	if (keysdown.down)
	{
		velocity.y += PLAYER_SPEED;
	}
	if (keysdown.left)
	{
		velocity.x -= PLAYER_SPEED;
	}
		
	if (keysdown.right)
	{
		velocity.x += PLAYER_SPEED;
	}

	//update position
	position.x += velocity.x*(timeStep / TIME_STEP_CORRECTION);
	position.y += velocity.y*(timeStep / TIME_STEP_CORRECTION);
}

void Minion::draw(SDL_Renderer* renderer)
{
	//match shape position to actual position
	shape.x = position.x;
	shape.y = position.y;
	//Render shape
	SDL_SetRenderDrawColor(renderer, colour.r, colour.g, colour.b, colour.a);
	SDL_RenderFillRect(renderer, &shape);
}

void Minion::deadReckoning(float timeStep)
{
	position.x += velocity.x*(timeStep / TIME_STEP_CORRECTION);
	position.y += velocity.y*(timeStep / TIME_STEP_CORRECTION);
}