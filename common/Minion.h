#pragma once
#include "SDL.h"
#include "common.h"

//some colours
//0, 130, 255 | 255, 100, 0 | 50, 255, 0 | 175, 50, 255

class Minion
{
#define TIME_STEP_CORRECTION 20000//slow down game

public:
	Minion();
	~Minion();
	
	void init(vec2 position, rgbaColour colour);//set start position and colour
	void update(KEYFLAGS keysdown, float timeStep);//get input, update velocity and position
	void draw(SDL_Renderer* renderer);//draw shape at position
	
	//position prediction for non player controlled minions
	void deadReckoning(float timestep);

	//getters and setters
	inline void setId(int id){ playerId = id;};
	inline void setPosition(vec2 pos){position = pos; };
	inline void setVelocity(vec2 vel){ velocity = vel; };

	inline bool isReady() { return(playerId != -1); };//make sure we have an id (connection to server was successful)
	inline int getId(){ return playerId;};
	inline vec2 getPosition(){ return position; };
	inline vec2 getVelocity(){ return velocity; };
	inline SDL_Rect getRect(){ return shape; };

private:
	vec2 velocity, position;
	rgbaColour colour;
	SDL_Rect shape;//shape drawn on screen
	int playerId;
};