#pragma once
#include "SDL_net.h"
#include "minion.h"
#include "TCPstream.h"
#include <vector>
#include "common.h"

class GameStream
{
public:
	GameStream();
	~GameStream();

	//init connection with server
	bool open(const char* ipAddress);

	//send position and velocity data
	bool send(Minion* player);

	//receive game info through server, ret false if error
	bool recv(std::vector<Minion*>& enemies, Minion* player);

	//notify server we're leaving and free sockets
	void close(Minion* player);

private:
	TCPstream tcps;
	data sendData, recvData;
	TCPsocket serverSocket;
	SDLNet_SocketSet server;//we only have 1 socket, but we use a socket set for its non blocking functionality
};