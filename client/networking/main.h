#pragma once
#include <stdio.h>
#include "SDL.h"
#include "SDL_net.h"
#include "LTimer.h"
#include "GameStream.h"
#include "Minion.h"
#include "common.h"
#include <ctime>

//sdl
SDL_Window* window = NULL;//The window
SDL_Renderer* renderer = NULL;//The window renderer

//networking
GameStream connection;

//game
KEYFLAGS keysdown;
Minion player;
std::vector<Minion*> enemies;
ground platform;

//functions
bool init();//Starts up SDL and creates window
void close();//Frees media and shuts down SDL
bool handleInput();//handle input events, ret true if quit