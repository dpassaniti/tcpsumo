#include "main.h"

int main(int argc, char* args[])
{
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\nPress enter to quit...");
		getchar();
	}
	else
	{
		srand(time(NULL));
		bool quit = false;
		float timeStep = 0;
		float timeToTalk = 0;//to check if enough time has passed since last comm and it's time to send another packet
		LTimer stepTimer;

		//init player with random position
		//player.init(vec2(100 + rand() % (SCREEN_WIDTH - 200), 100 + rand() % (SCREEN_HEIGHT - 200)), rgbaColour(PLAYER_COLOUR));
		player.init(vec2(100 + rand() % (SCREEN_WIDTH - 200), 100 + rand() % (SCREEN_HEIGHT - 200)), rgbaColour(PLAYER_COLOUR));

		while (!quit)
		{
			quit = handleInput();//Handle events on queue

			if (keysdown.zero)//TESTING PURPOSES///////
			{
				player.setVelocity(vec2(0, 0));
			}//////////////////////////////////////////

			//update player
			timeStep = stepTimer.getTicks();//get time in milliseconds since last frame
			player.update(keysdown, timeStep);
			stepTimer.start();//Restart step timer

			//communicate with server
			if ((timeToTalk+=timeStep)>= 50)//send update 20 times a second
			{
				connection.send(&player);//Send player info to server
				timeToTalk = 0;
			}
			connection.recv(enemies, &player);//receive messages from server and act accordingly
			
			//Clear creen
			SDL_SetRenderDrawColor(renderer, BACKGROUND_COLOUR);
			SDL_RenderClear(renderer);
			//draw ground
			SDL_SetRenderDrawColor(renderer, platform.colour.r, platform.colour.g, platform.colour.b, platform.colour.a);
			SDL_RenderFillRect(renderer, &platform.rect);
			//draw player
			player.draw(renderer);
			//draw all enemies at their predicted positions
			for (int i = 0; i < enemies.size(); ++i)
			{
				enemies[i]->deadReckoning(timeStep);
				enemies[i]->draw(renderer);
			}
			//update scren
			SDL_RenderPresent(renderer);
		}
		if (player.isReady())//we were connected
			connection.close(&player);//close connection
	}

	//Free resources and close SDL
	close();

	return 0;
}

bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else if (SDLNet_Init() < 0)
	{
		printf("SDL_Net could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else if(!connection.open(SERVERIP))//init connection		
	{
		printf("Could not establish a connection\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Create window
		window = SDL_CreateWindow("client", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (window == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create renderer for window
			renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
			if (renderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Initialize renderer colour
				SDL_SetRenderDrawColor(renderer, BACKGROUND_COLOUR);
			}
		}
	}
	return success;
}

void close()
{
	//free window and renderer	
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	window = NULL;
	renderer = NULL;

	//Quit SDL subsystems
	SDLNet_Quit();
	SDL_Quit();
}

bool handleInput()
{
	SDL_Event e;//Event handler

	while (SDL_PollEvent(&e) != 0)
	{
		//keydown
		if (e.type == SDL_KEYDOWN)
		{
			switch (e.key.keysym.sym)
			{
			case SDLK_UP:
				keysdown.up = true;
				break;
			case SDLK_DOWN:
				keysdown.down = true;
				break;
			case SDLK_LEFT:
				keysdown.left = true;
				break;
			case SDLK_RIGHT:
				keysdown.right = true;
				break;
			case SDLK_ESCAPE:
				return true;
				break;
			case SDLK_SPACE:
				keysdown.space = true;
				break;
			case SDLK_0:
				keysdown.zero = true;
			default:
				break;
			}
		}
		//keyup
		if (e.type == SDL_KEYUP)
		{
			switch (e.key.keysym.sym)
			{
			case SDLK_UP:
				keysdown.up = false;
				break;
			case SDLK_DOWN:
				keysdown.down = false;
				break;
			case SDLK_LEFT:
				keysdown.left = false;
				break;
			case SDLK_RIGHT:
				keysdown.right = false;
				break;
			case SDLK_SPACE:
				keysdown.space = false;
			case SDLK_0:
				keysdown.zero = false;
			default:
				break;
			}
		}
		//User requests quit
		else if (e.type == SDL_QUIT)
		{
			return true;
		}
	}
	return false;//no quit event
}