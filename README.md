--------------HOW TO PLAY---------------------------------

start the server
start the client(s)

CLIENT:
move with arrow keys
don't exit the black area or you will lose
colliding with another player will swap your velocity with his and vice versa,
	use this to throw them out of the black area
ESC to quit

SERVER:
spectate the game with no dead reckoning applied
ESC to quit

---------------EXTERNAL LIBRARIES------------------------

SDL: https://www.libsdl.org/download-2.0.php
SDL_Net: https://www.libsdl.org/projects/SDL_net/

---------------REFERENCES--------------------------------

LTimer class and separating axis collision check method adapted from
	SDL tutorials at http://lazyfoo.net/tutorials/SDL/index.php

-------note----------
Bug noticed on library computer only:
when a client is eliminated, he is correctly notified he lost and is disconnected, but then he stops responding.
This does not affect the rest of the connections.